// Timer for content delivery
// Source: https://stackoverflow.com/questions/26049855/javascript-time-counter-since-start-date-and-time

var startDateTime = new Date(2007,2,13,20,40,36,0); // YYYY (M-1) D H m s ms (start const startStamp = startDateTime.getTime();

var startStamp = startDateTime.getTime();

var newDate = new Date();
var newStamp = newDate.getTime();

var timer;

function updateClock() {
    newDate = new Date();
    newStamp = newDate.getTime();
    var diff = Math.round((newStamp-startStamp)/1000);
    
    var d = Math.floor(diff/(24*60*60));
    diff = diff-(d*24*60*60);
    var h = Math.floor(diff/(60*60));
    diff = diff-(h*60*60);
    var m = Math.floor(diff/(60));
    diff = diff-(m*60);
    var s = diff;
    
    document.getElementById("beginning-of-blog").innerHTML = "Content Delivery for: "+d+" day(s), "+h+" hour(s), "+m+" minute(s), "+s+" second(s)";
}

timer = setInterval(updateClock, 1000);